function HatList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Hat Fabric</th>
            <th>Hat Color</th>
            <th>Hat Style</th>
            <th>Hat Location</th>
          </tr>
        </thead>
        <tbody>
          {props.state.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric}</td>
                <td>{ hat.color }</td>
                <td>{ hat.style }</td>
                <td>{ hat.locationVO }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;