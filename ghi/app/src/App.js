import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList'
import ShoeForm from './ShoeForm';
import HatList from './HatList';


function App(props) {
  if (props.shoes === undefined) {
    return <></>;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage shoes={props.shoes}/>} />
          <Route path="shoes" element={<ShoesList shoes={props.shoes}/>} />
          <Route path="shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
