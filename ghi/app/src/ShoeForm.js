import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            model: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.bins;

        const shoeURL = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeURL, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()

            const clearForm = {
                manufacturer: '',
                model: '',
                color: '',
                picture_url: '',
                bin: '',
            };
            this.setState(clearForm);
        }
    }

    handleFieldChange = (event) => {
        event.preventDefault();
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        });
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/bins/');
        
        if(response.ok) {
            const data = await response.json();

            this.setState({
                bins: data.bins
            })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe!</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFieldChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFieldChange} value={this.state.model} placeholder="Model" required type="text" name="model" className="form-control"/>
                            <label htmlFor="model">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFieldChange} value={this.state.color} placeholder="Colors" required type="text" name="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFieldChange} value={this.state.picture_url} placeholder="Picture" required type="text" name="picture_url" className="form-control"/>
                            <label htmlFor="picture_url">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleFieldChange} value={this.state.bin} id="select-bin" required name="bin" className="form-select">
                            <option value="default">Choose a bin</option>
                            {this.state.bins.map(bin => {
                                return(
                                    <option key={bin.id} value={bin.id}>
                                        {bin.closet_name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}

export default ShoeForm