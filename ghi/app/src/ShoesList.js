function ShoesList(props) {
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Manufacturer</th>
                <th>Model</th>
                <th>Color</th>
                <th>Bin</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            {props.shoes.map(shoe => {
                return(
                <tr key={shoe.id}>
                    <td>
                        <img 
                            src={shoe.picture_url}
                            alt='loading failed'
                            height='75px'
                            width='100px'
                        />
                    </td>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.model}</td>
                    <td>{shoe.color}</td>
                    <td>{shoe.bin}</td>
                    <td>
                        <button type="submit">

                        </button>
                    </td>
                </tr>
                )
            })}
            </tbody>
        </table>
    );
}

export default ShoesList