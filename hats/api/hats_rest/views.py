from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hats, LocationVO
from django.db import IntegrityError
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "picture_URL",
        "location",
        ]
    encoders = {'location': LocationVOEncoder() }

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "picture_URL",
        "location",
        ]
    def get_extra_data(self, o):
        return {"location" : o.location.closet_name}






# def create_hat(json_content):
    # try:
    #     content = json.loads(json_content)
    # except json.JSONDecodeError:
    #     return 400, {"message": "Bad JSON"}, None

    # required_properties = [
    #     "fabric",
    #     "style",
    #     "color",
    #     "picture_URL",
    #     "location",
    # ]

    # missing_properties = []
    # for required_property in required_properties:
    #     if (
    #         required_property not in content
    #         or len(content[required_property]) == 0
    #     ):
    #         missing_properties.append(required_property)
    # if missing_properties:
    #     response_content = {
    #         "message": "missing properties",
    #         "properties": missing_properties,
    #     }
    #     return 400, response_content, None

    # try:
    #     hat = Hats.objects.create_hat(
    #         fabric=content["fabic"],
    #         style=content["style"],
    #         color=content["color"],
    #         picture_URL=content["picture_URL"],
    #         location=content["location"],
    #     )
    #     return 200, hat, hat
    # except IntegrityError as e:
    #     return 409, {"message": str(e)}, None
    # except ValueError as e:
    #     return 400, {"message": str(e)}, None


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse (
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
                location_href = content['location']
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location

        except LocationVO.DoesNotExist:

            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

        # status_code, response_content, _ = create_hat(request.body)
        # if status_code >= 200 and status_code < 300:
        #     send_account_data(response_content)
        # response = JsonResponse(
        #     response_content,
        #     encoder=HatListEncoder,
        #     safe=False,
        # )
        # response.status_code = status_code
        # return response
