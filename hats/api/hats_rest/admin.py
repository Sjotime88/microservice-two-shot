from django.contrib import admin
from .models import Hats, LocationVO

admin.site.register(Hats)
admin.site.register(LocationVO)
