from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length = 100, unique=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    closet_name= models.CharField(max_length=100, null=True)
    shelf_number=models.PositiveSmallIntegerField(null=True)


class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_URL = models.URLField(null=True)
    location = models.ForeignKey('LocationVO', related_name='location', on_delete=models.CASCADE)

    def __str__(self):
        return self.model
