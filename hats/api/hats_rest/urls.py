from django.urls import path
from .views import api_list_hats


urlpatterns = [
    #Get request - Hats List Request
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="api_list_hat"),

    #Post request - Hats Post Request
    path("hats/", api_list_hats, name="api_create_hat")

]