from django.http import JsonResponse
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["bin_number", "import_href", "closet"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ['id', 'model', 'color', 'picture_url', 'manufacturer']

    def get_extra_data(self, o):
        return {'bin': o.bin.closet}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'id',
        'manufacturer',
        'model',
        'color',
        'picture_url',
        'bin',
    ]
    encoders = {'bin': BinVOEncoder()}

@require_http_methods(["POST", "GET"])
def api_shoe_list(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id != None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else: 
            shoes = Shoe.objects.all()    
        return JsonResponse(
            {'shoes': shoes},
            encoder = ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            if  bin_vo_id != None:
                bin_href = f'/api/locations/{bin_vo_id}/'
            else: 
                bin_href = f'/api/locations/{content["bin"]}/'    
            bin = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin 
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin id"}, status=400,)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder = ShoeDetailEncoder, safe=False,)        

@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe_details(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id = pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder = ShoeDetailEncoder,
            safe = False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Shoe.objects.filter(id = pk).update(**content)   
        shoe = Shoe.objects.get(id = pk)
        return JsonResponse(
            shoe, 
            encoder = ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})    
