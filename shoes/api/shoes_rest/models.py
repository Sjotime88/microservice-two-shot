from django.db import models
from django.urls import reverse

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100, null=True)
    model = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey('BinVO', related_name="shoes", on_delete=models.CASCADE)

    def __str__(self):
        return self.model

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})    


class BinVO(models.Model):
    import_href = models.CharField(max_length=100, null=True)
    bin_number = models.SmallIntegerField(null=True)
    closet = models.CharField(max_length=100, null=True)