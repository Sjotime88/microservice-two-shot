from django.urls import path
from .views import api_shoe_list, api_shoe_details

urlpatterns = [
    path("bins/<int:bin_vo_id>/shoes/", api_shoe_list, name="api_list_shoes"),
    path("shoes/", api_shoe_list, name="api_create_shoe"),
    path("shoes/<int:pk>/", api_shoe_details, name="api_shoe_details"),
]