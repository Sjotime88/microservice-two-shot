# Wardrobify

Team:

* America - Shoes
* Ben - Hat

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

There are several properties of an individual hat that can be entered and saved including it's location. A hat's location can have several other properties all of which can be entered and saved as well.
